# How to use this image once built

How to launch the image:

```bash
sudo docker run --rm --name alpine-dev-1 -v $PWD/source:/home/user1/aports -v $PWD/build:/data -it gounthar/cabal-dev:aarch64
```

Once you're in, `cd ~/alpine-ghc-aarch64` and then issue `sudo ./builder.sh` .
