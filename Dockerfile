FROM alpine:latest

# install required packages to build "alpine linux" packages
RUN apk add --update --no-cache --no-progress alpine-sdk coreutils bash
RUN apk add --update --no-cache --no-progress sudo jq

# setup directory for built packages
RUN mkdir -p /var/cache/distfiles
RUN chmod a+w /var/cache/distfiles
RUN chgrp abuild /var/cache/distfiles
RUN chmod g+w /var/cache/distfiles

# setup the abuild configuration
RUN echo 'PACKAGER="Your Name <your@email.address>"' >> /etc/abuild.conf
RUN echo 'MAINTAINER="$PACKAGER"' >> /etc/abuild.conf
RUN echo "%abuild ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/abuild

# setup the build user in container
RUN adduser -D user1
RUN addgroup user1 abuild

RUN mkdir -p /home/user1/aports && \
# clone the aports repo
    cd /home/user1 && git clone git://git.alpinelinux.org/aports && cd /home/user1/aports && git pull && \
    chown -R user1:user1 /home/user1 

# setup directory to pass in build instructions files for abuild
VOLUME /home/user1/aports
WORKDIR /home/user1/aports

# setup directory to host the resulting apks
VOLUME /data
RUN mkdir -p /data && chown -R user1:user1 /data

# make user1 the current user in the image
USER user1


# create keys for signing packages after the build has been finished
RUN abuild-keygen -a -i -n

# setup git for the build user
RUN git config --global user.name "Build User"
RUN git config --global user.email "build-user@example.com"

# up to here, I changed almost nothing
# now, let's use rubikscraft work to download the ghc bootstrap binaries
RUN cd /home/user1/ && git clone https://github.com/rubikscraft/alpine-ghc-aarch64.git && \
  cd alpine-ghc-aarch64/ && \
  ln -s /home/user1/aports/ aports && \
  chmod +x *.sh && ./download_bootstrap.sh && \
  sudo apk add --allow-untrusted ghc-*apk && cd ..
